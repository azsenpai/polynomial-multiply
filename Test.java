import java.io.FileWriter;
import java.io.BufferedWriter;
import algorithm.IMultiply;
import algorithm.MultiplyUsual;
import algorithm.MultiplyKaratsuba;
import algorithm.MultiplyBigInteger;
import algorithm.MultiplyFFTRecursive;
import algorithm.MultiplyFFTIterative;

/**
 * @author zh.adlet@gmail.com
 */
public class Test
{
    /**
     *
     */
    public static void printUsage()
    {
        String usage = "";

        usage += "Usage: java Test <methodNumber> <length>\n\n";

        usage += "<methodNumber> can be:\n";
        usage += "  0 - Usual\n";
        usage += "  1 - Karatsuba\n";
        usage += "  2 - FFT Recursive\n";
        usage += "  3 - FFT Iterative\n";
        usage += "  4 - BigInteger\n\n";

        usage += "<length> length of number, e.g. 3 will multiply 999*999";

        System.out.println(usage);
    }

    /**
     *
     */
    public static void main(String[] args)
    {
        if (args.length < 2) {
            printUsage();
            return;
        }

        int method, n;

        try {
            method = Integer.parseInt(args[0]);
            n = Integer.parseInt(args[1]);
        } catch (Exception e) {
            printUsage();
            return;
        }

        IMultiply algorithm;

        switch (method) {
            case 0:
                algorithm = new MultiplyUsual();
                break;

            case 1:
                algorithm = new MultiplyKaratsuba();
                break;

            case 2:
                algorithm = new MultiplyFFTRecursive();
                break;

            case 3:
                algorithm = new MultiplyFFTIterative();
                break;

            case 4:
                algorithm = new MultiplyBigInteger();
                break;

            default:
                printUsage();
                return;
        }

        String strNumberA = String.format("%0" + n + "d", 9).replace('0', '9');
        String strNumberB = String.format("%0" + n + "d", 9).replace('0', '9');

        long startTime = System.nanoTime();
        String strNumberC = algorithm.multiply(strNumberA, strNumberB);
        long endTime = System.nanoTime();

        String methodName = algorithm.getName();

        System.out.println(String.format("%s method (elapsed time): ", methodName) + (endTime - startTime) / 1000000.0 + "ms");

        try {
            String filename = String.format("output-%s.txt", methodName);
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));

            writer.write(strNumberC);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
