package algorithm;

/**
 * @author zh.adlet@gmail.com
 */
public interface IMultiply
{
    /**
     *
     */
    public String getName();

    /**
     *
     */
    public String multiply(String strNumberA, String strNumberB);
}
