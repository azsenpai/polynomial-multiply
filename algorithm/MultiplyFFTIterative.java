package algorithm;

import structure.Polynomial;
import structure.Complex;

/**
 * @author zh.adlet@gmail.com
 */
public class MultiplyFFTIterative implements IMultiply
{
    /**
     *
     */
    public static Polynomial<Complex> fft(Polynomial<Complex> a, boolean isInterpolation)
    {
        Polynomial<Complex> y = a.getBitReversedElements();

        final int n = a.length();
        final int k = (int)(Math.log(n) / Math.log(2));

        for (int s = 1; s <= k; s ++) {
            int m = (1 << s);
            int mdiv2 = (m >> 1);

            Complex wn = Complex.valueOf((isInterpolation ? -1 : 1) * 2*Math.PI / m);

            for (int i = 0; i < n; i += m) {
                Complex w = Complex.valueOf(1, 0);

                for (int j = 0; j < mdiv2; j ++) {
                    Complex u = y.get(i+j);
                    Complex v = y.get(i+j+mdiv2).multiply(w);

                    y.set(i+j, u.add(v));
                    y.set(i+j+mdiv2, u.subtract(v));

                    w = w.multiply(wn);
                }
            }
        }

        if (isInterpolation) {
            for (int i = 0; i < n; i ++) {
                y.set(i, y.get(i).divide(n));
            }
        }

        return y;
    }

    /**
     *
     */
    public static Polynomial<Integer> multiply(Polynomial<Integer> a, Polynomial<Integer> b)
    {
        int n = Polynomial.getPowerOfTwo(Math.max(a.length(), b.length()));
        n *= 2;

        a = a.slice(0, n);
        b = b.slice(0, n);

        Polynomial<Complex> ya = fft(a.toComplex(), false);
        Polynomial<Complex> yb = fft(b.toComplex(), false);

        Polynomial<Complex> yc = ya.dotMultiply(yb);

        return fft(yc, true).toInteger();
    }

    /**
     *
     */
    public String getName()
    {
        return "FFT-Iterative";
    }

    /**
     *
     */
    public String multiply(String strNumberA, String strNumberB)
    {
        Polynomial<Integer> a = Polynomial.valueOf(strNumberA);
        Polynomial<Integer> b = Polynomial.valueOf(strNumberB);

        Polynomial<Integer> c = multiply(a, b);

        return c.strNumberValue();
    }
}
