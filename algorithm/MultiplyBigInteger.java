package algorithm;

import java.math.BigInteger;

/**
 * @author zh.adlet@gmail.com
 */
public class MultiplyBigInteger implements IMultiply
{
    /**
     *
     */
    public static BigInteger valueOf(String strNumber)
    {
        BigInteger result = BigInteger.ZERO;

        final int radix = 10;
        final BigInteger x = BigInteger.valueOf(radix);

        final int n = strNumber.length();
        int digit;

        for (int i = 0; i < n; i ++) {
            digit = Character.digit(strNumber.charAt(i), radix);
            result = result.multiply(x).add(BigInteger.valueOf(digit));
        }

        return result;
    }

    /**
     *
     */
    public String getName()
    {
        return "BigInteger";
    }

    /**
     *
     */
    public String multiply(String strNumberA, String strNumberB)
    {
        BigInteger a = valueOf(strNumberA);
        BigInteger b = valueOf(strNumberB);

        BigInteger c = a.multiply(b);

        return c.toString();
    }
}
