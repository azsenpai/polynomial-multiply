package algorithm;

import structure.Polynomial;

/**
 * @author zh.adlet@gmail.com
 */
public class MultiplyKaratsuba implements IMultiply
{
    /**
     *
     */
    public static Polynomial<Integer> multiply(Polynomial<Integer> P, Polynomial<Integer> Q)
    {
        int n = Math.max(P.length(), Q.length());
        n += n % 2;

        if (n <= 100) {
            return P.multiply(Q);
        }

        final int m = n / 2;

        Polynomial<Integer> a = P.slice(m, m);
        Polynomial<Integer> b = P.slice(0, m);
        Polynomial<Integer> c = Q.slice(m, m);
        Polynomial<Integer> d = Q.slice(0, m);

        Polynomial<Integer> p = multiply(a, c);
        Polynomial<Integer> q = multiply(b, d);
        Polynomial<Integer> r = multiply(a.add(b), c.add(d)).subtract(p).subtract(q);

        return p.lshift(n).add(r.lshift(m)).add(q);
    }

    /**
     *
     */
    public String getName()
    {
        return "Karatsuba";
    }

    /**
     *
     */
    public String multiply(String strNumberA, String strNumberB)
    {
        Polynomial<Integer> a = Polynomial.valueOf(strNumberA);
        Polynomial<Integer> b = Polynomial.valueOf(strNumberB);

        Polynomial<Integer> c = multiply(a, b);

        return c.strNumberValue();
    }
}
