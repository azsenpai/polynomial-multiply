package algorithm;

import structure.Polynomial;

/**
 * @author zh.adlet@gmail.com
 */
public class MultiplyUsual implements IMultiply
{
    /**
     *
     */
    public static Polynomial<Integer> multiply(Polynomial<Integer> p, Polynomial<Integer> q)
    {
        return p.multiply(q);
    }

    /**
     *
     */
    public String getName()
    {
        return "Usual";
    }

    /**
     *
     */
    public String multiply(String strNumberA, String strNumberB)
    {
        Polynomial<Integer> a = Polynomial.valueOf(strNumberA);
        Polynomial<Integer> b = Polynomial.valueOf(strNumberB);

        Polynomial c = multiply(a, b);

        return c.strNumberValue();
    }
}
