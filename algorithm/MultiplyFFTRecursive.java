package algorithm;

import structure.Polynomial;
import structure.Complex;

/**
 * @author zh.adlet@gmail.com
 */
public class MultiplyFFTRecursive implements IMultiply
{
    /**
     *
     */
    public static Polynomial<Complex> fft(Polynomial<Complex> a, boolean isInterpolation)
    {
        final int n = a.length();
        final int m = n / 2;

        if (n <= 1) {
            return a;
        }

        Polynomial<Complex> y0 = fft(a.getA0(), isInterpolation);
        Polynomial<Complex> y1 = fft(a.getA1(), isInterpolation);

        Polynomial<Complex> y = new Polynomial<Complex>(new Complex[n]);

        Complex w = Complex.valueOf(1, 0);
        Complex wn = Complex.valueOf((isInterpolation ? -1 : 1) * 2*Math.PI / n);

        Complex u, v;

        for (int i = 0; i < m; i ++) {
            u = y0.get(i);
            v = y1.get(i).multiply(w);

            y.set(i, u.add(v));
            y.set(m + i, u.subtract(v));

            if (isInterpolation) {
                y.set(i, y.get(i).divide(2));
                y.set(m + i, y.get(m + i).divide(2));
            }

            w = w.multiply(wn);
        }

        return y;
    }

    /**
     *
     */
    public static Polynomial<Integer> multiply(Polynomial<Integer> a, Polynomial<Integer> b)
    {
        int n = Polynomial.getPowerOfTwo(Math.max(a.length(), b.length()));
        n *= 2;

        a = a.slice(0, n);
        b = b.slice(0, n);

        Polynomial<Complex> ya = fft(a.toComplex(), false);
        Polynomial<Complex> yb = fft(b.toComplex(), false);

        Polynomial<Complex> yc = ya.dotMultiply(yb);

        return fft(yc, true).toInteger();
    }

    /**
     *
     */
    public String getName()
    {
        return "FFT-Recursive";
    }

    /**
     *
     */
    public String multiply(String strNumberA, String strNumberB)
    {
        Polynomial<Integer> a = Polynomial.valueOf(strNumberA);
        Polynomial<Integer> b = Polynomial.valueOf(strNumberB);

        Polynomial<Integer> c = multiply(a, b);

        return c.strNumberValue();
    }
}
