package structure;

/**
 * @author zh.adlet@gmail.com
 */
public class PolynomialInteger
{
    /**
     *
     */
    public static Polynomial<Integer> valueOf(String strNumber, int x)
    {
        final int n = strNumber.length();
        Integer[] a = new Integer[n];

        for (int i = 0; i < n; i ++) {
            a[i] = Character.digit(strNumber.charAt(n-i-1), x);
        }

        return new Polynomial<Integer>(a, x);
    }

    /**
     *
     */
    public static String strNumberValue(Polynomial<Integer> p)
    {
        final Integer[] a = p.getA();
        final int x = p.getX();

        int n = a.length;
        int carry = 0;

        for (int i = 0; i < a.length; i ++) {
            carry = (a[i] + carry) / x;
        }

        for (; carry > 0; carry /= x) {
            n ++;
        }

        int[] result = new int[n];
        carry = 0;

        for (int i = 0; i < a.length; i ++) {
            result[i] = a[i] + carry;
            carry = result[i] / x;
            result[i] %= x;
        }

        for (int i = a.length; i < n; i ++) {
            result[i] = carry % x;
            carry /= x;
        }

        while (n > 0 && result[n-1] == 0) {
            n --;
        }

        String strNumber = "";

        for (int i = 0; i < n; i ++) {
            strNumber += result[n-i-1];
        }

        return strNumber;
    }

    /**
     *
     */
    public static Polynomial<Complex> toComplex(Polynomial<Integer> p)
    {
        Complex[] a = new Complex[p.length()];

        for (int i = 0; i < p.length(); i ++) {
            a[i] = Complex.valueOf(p.get(i), 0);
        }

        return new Polynomial<Complex>(a, p.getX());
    }

    /**
     *
     */
    public static Polynomial<Integer> add(Polynomial<Integer> p, Polynomial<Integer> q)
    {
        if (p.getX() != q.getX()) {
            return null;
        }

        final int n = Math.max(p.length(), q.length());
        Integer[] a = new Integer[n];

        for (int i = 0; i < n; i ++) {
            a[i] = p.get(i) + q.get(i);
        }

        return new Polynomial<Integer>(a, p.getX());
    }

    /**
     *
     */
    public static Polynomial<Integer> subtract(Polynomial<Integer> p, Polynomial<Integer> q)
    {
        if (p.getX() != q.getX()) {
            return null;
        }

        final int n = Math.max(p.length(), q.length());
        Integer[] a = new Integer[n];

        for (int i = 0; i < n; i ++) {
            a[i] = p.get(i) - q.get(i);
        }

        return new Polynomial<Integer>(a, p.getX());
    }

    /**
     *
     */
    public static Polynomial<Integer> multiply(Polynomial<Integer> p, Polynomial<Integer> q)
    {
        if (p.getX() != q.getX()) {
            return null;
        }

        final int n = p.length() + q.length() - 1;
        Integer[] a = new Integer[n];

        for (int i = 0; i < n; i ++) {
            a[i] = 0;
        }

        for (int j = 0; j < q.length(); j ++) {
            for (int i = 0; i < p.length(); i ++) {
                a[i+j] += p.get(i) * q.get(j);
            }
        }

        return new Polynomial<Integer>(a, p.getX());
    }

    /**
     *
     */
    public static Polynomial<Integer> dotMultiply(Polynomial<Integer> p, Polynomial<Integer> q)
    {
        if (p.getX() != q.getX()) {
            return null;
        }

        final int n = Math.max(p.length(), q.length());
        Integer[] a = new Integer[n];

        for (int i = 0; i < n; i ++) {
            a[i] = p.get(i) * q.get(i);
        }

        return new Polynomial<Integer>(a, p.getX());
    }
}
