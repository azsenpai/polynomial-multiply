package structure;

/**
 * @author zh.adlet@gmail.com
 */
public class Complex extends Number
{
    /**
     *
     */
    private double x;

    /**
     *
     */
    private double y;

    /**
     *
     */
    public Complex(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     *
     */
    public Complex(double phi)
    {
        this.x = Math.cos(phi);
        this.y = Math.sin(phi);
    }

    /**
     *
     */
    public static Complex valueOf(double x, double y)
    {
        return new Complex(x, y);
    }

    /**
     *
     */
    public static Complex valueOf(double phi)
    {
        return new Complex(phi);
    }

    /**
     *
     */
    public String toString()
    {
        return String.format("(%f, %f)", x, y);
    }

    /**
     *
     */
    public double real()
    {
        return x;
    }

    /**
     *
     */
    public double imaginary()
    {
        return y;
    }

    /**
     *
     */
    public boolean isZero()
    {
        return x == 0 && y == 0;
    }

    /**
     *
     */
    public Complex add(Complex other)
    {
        return new Complex(x + other.x, y + other.y);
    }

    /**
     *
     */
    public Complex subtract(Complex other)
    {
        return new Complex(x - other.x, y - other.y);
    }

    /**
     *
     */
    public Complex multiply(Complex other)
    {
        return new Complex(x*other.x - y*other.y, x*other.y + y*other.x);
    }

    /**
     *
     */
    public Complex divide(double k)
    {
        return new Complex(x / k, y / k);
    }

    /**
     *
     */
    public double doubleValue()
    {
        return x;
    }

    /**
     *
     */
    public float floatValue()
    {
        return (float)x;
    }

    /**
     *
     */
    public int intValue()
    {
        return (int)x;
    }

    /**
     *
     */
    public long longValue()
    {
        return (long)x;
    }
}
