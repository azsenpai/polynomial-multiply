package structure;

/**
 * @author zh.adlet@gmail.com
 */
public class PolynomialComplex
{
    /**
     *
     */
    public static Polynomial<Integer> toInteger(Polynomial<Complex> p)
    {
        Integer[] a = new Integer[p.length()];

        for (int i = 0; i < p.length(); i ++) {
            a[i] = (int)(p.get(i).real() + 0.5);
        }

        return new Polynomial<Integer>(a, p.getX());
    }

    /**
     *
     */
    public static Polynomial<Complex> add(Polynomial<Complex> p, Polynomial<Complex> q)
    {
        if (p.getX() != q.getX()) {
            return null;
        }

        final int n = Math.max(p.length(), q.length());
        Complex[] a = new Complex[n];

        for (int i = 0; i < n; i ++) {
            a[i] = p.get(i).add(q.get(i));
        }

        return new Polynomial(a, p.getX());
    }

    /**
     *
     */
    public static Polynomial<Complex> subtract(Polynomial<Complex> p, Polynomial<Complex> q)
    {
        if (p.getX() != q.getX()) {
            return null;
        }

        final int n = Math.max(p.length(), q.length());
        Complex[] a = new Complex[n];

        for (int i = 0; i < n; i ++) {
            a[i] = p.get(i).subtract(q.get(i));
        }

        return new Polynomial(a, p.getX());
    }

    /**
     *
     */
    public static Polynomial<Complex> multiply(Polynomial<Complex> p, Polynomial<Complex> q)
    {
        if (p.getX() != q.getX()) {
            return null;
        }

        final int n = p.length() + q.length() - 1;
        Complex[] a = new Complex[n];

        for (int i = 0; i < n; i ++) {
            a[i] = Complex.valueOf(0, 0);
        }

        for (int j = 0; j < q.length(); j ++) {
            for (int i = 0; i < p.length(); i ++) {
                a[i+j] = a[i+j].add(p.get(i).multiply(q.get(j)));
            }
        }

        return new Polynomial(a, p.getX());
    }

    /**
     *
     */
    public static Polynomial<Complex> dotMultiply(Polynomial<Complex> p, Polynomial<Complex> q)
    {
        if (p.getX() != q.getX()) {
            return null;
        }

        final int n = Math.max(p.length(), q.length());
        Complex[] a = new Complex[n];

        for (int i = 0; i < n; i ++) {
            a[i] = p.get(i).multiply(q.get(i));
        }

        return new Polynomial(a, p.getX());
    }
}
