package structure;

/**
 * @author zh.adlet@gmail.com
 */
public class Polynomial<N extends Number>
{
    /**
     *
     */
    protected N[] a;

    /**
     *
     */
    protected int x;

    /**
     *
     */
    protected boolean isInteger;

    /**
     *
     */
    protected boolean isComplex;

    /**
     *
     */
    protected N zeroValue;

    /**
     *
     */
    public Polynomial(N[] a, int x)
    {
        init(a, x);
    }

    /**
     *
     */
    public Polynomial(N[] a)
    {
        init(a, 10);
    }

    /**
     *
     */
    protected void init(N[] a, int x)
    {
        this.a = (a != null) ? a.clone() : (N[])(new Integer[]{});
        this.x = x;

        isInteger = (a instanceof Integer[]);
        isComplex = (a instanceof Complex[]);

        zeroValue = null;

        if (isInteger) {
            zeroValue = (N)Integer.valueOf(0);
        } else if (isComplex) {
            zeroValue = (N)Complex.valueOf(0, 0);
        }
    }

    /**
     *
     */
    public static int getPowerOfTwo(int n)
    {
        int result = 1;

        while (result < n) {
            result <<= 1;
        }

        return result;
    }

    /**
     *
     */
    public static Polynomial<Integer> valueOf(String strNumber, int x)
    {
        return PolynomialInteger.valueOf(strNumber, x);
    }

    /**
     *
     */
    public static Polynomial<Integer> valueOf(String strNumber)
    {
        return valueOf(strNumber, 10);
    }

    /**
     *
     */
    public static int rev(int n, int bits)
    {
        int result = 0;

        for (int i = 0; i < bits; i ++) {
            if (n % 2 > 0) {
                result += (1 << (bits - i - 1));
            }
            n >>= 1;
        }

        return result;
    }

    /**
     *
     */
    public String toString()
    {
        String result = "";

        result += "(";

        if (a.length > 0) {
            result += a[0];
        }

        for (int i = 1; i < a.length; i ++) {
            result += ", " + a[i];
        }

        result += ")";

        return result;
    }

    /**
     *
     */
    public String strNumberValue()
    {
        if (isInteger) {
            return PolynomialInteger.strNumberValue((Polynomial<Integer>)this);
        }

        return null;
    }

    /**
     *
     */
    public int length()
    {
        return a.length;
    }

    /**
     *
     */
    public N get(int index, N value)
    {
        return (index < a.length) ? a[index] : value;
    }

    /**
     *
     */
    public N get(int index)
    {
        return get(index, zeroValue);
    }

    /**
     *
     */
    public N set(int index, N value)
    {
        if (index < a.length) {
            a[index] = value;
            return value;
        }

        return null;
    }

    /**
     *
     */
    public N[] getA()
    {
        return a;
    }

    /**
     *
     */
    public int getX()
    {
        return x;
    }

    /**
     *
     */
    public N[] getArray(int n, N value)
    {
        N[] a = null;

        if (isInteger) {
            a = (N[])(new Integer[n]);
        } else if (isComplex) {
            a = (N[])(new Complex[n]);
        }

        if (a != null) {
            for (int i = 0; i < n; i ++) {
                a[i] = value;
            }
        }

        return a;
    }

    /**
     *
     */
    public N[] getArray(int n)
    {
        return getArray(n, zeroValue);
    }

    /**
     *
     */
    public Polynomial<N> slice(int start, int count)
    {
        if (count < 0) {
            return null;
        }

        N[] a = getArray(count);

        for (int i = 0; i < count; i ++) {
            a[i] = get(start + i);
        }

        return new Polynomial<N>(a, x);
    }

    /**
     *
     */
    public Polynomial<N> lshift(int count)
    {
        if (count < 0) {
            return null;
        }

        N[] a = getArray(this.a.length + count);

        for (int i = 0; i < this.a.length; i ++) {
            a[count+i] = this.a[i];
        }

        return new Polynomial<N>(a, x);
    }

    /**
     *
     */
    public Polynomial<N> getElements(boolean isEven)
    {
        final int k = isEven ? 0 : 1;
        final int n = (this.a.length + 1 - k) / 2;

        N[] a = getArray(n);

        for (int i = 0, j = k; i < n; i ++, j += 2) {
            a[i] = this.a[j];
        }

        return new Polynomial<N>(a);
    }

    /**
     *
     */
    public Polynomial<N> getA0()
    {
        return getElements(true);
    }

    /**
     *
     */
    public Polynomial<N> getA1()
    {
        return getElements(false);
    }

    /**
     *
     */
    public Polynomial<N> getBitReversedElements()
    {
        final int n = a.length;
        final int k = (int)(Math.log(n) / Math.log(2));

        N[] result = getArray(n);

        for (int i = 0; i < n; i ++) {
            result[rev(i, k)] = a[i];
        }

        return new Polynomial<N>(result, x);
    }

    /**
     *
     */
    public Polynomial<Integer> toInteger()
    {
        if (isInteger) {
            return new Polynomial<Integer>((Integer[])a, x);
        }

        if (isComplex) {
            return PolynomialComplex.toInteger((Polynomial<Complex>)this);
        }

        return null;
    }

    /**
     *
     */
    public Polynomial<Complex> toComplex()
    {
        if (isComplex) {
            return new Polynomial<Complex>((Complex[])a, x);
        }

        if (isInteger) {
            return PolynomialInteger.toComplex((Polynomial<Integer>)this);
        }

        return null;
    }

    /**
     *
     */
    public Polynomial<N> add(Polynomial<N> other)
    {
        if (isInteger && other.isInteger) {
            return (Polynomial<N>)PolynomialInteger.add((Polynomial<Integer>)this, (Polynomial<Integer>)other);
        }

        if (isComplex && other.isComplex) {
            return (Polynomial<N>)PolynomialComplex.add((Polynomial<Complex>)this, (Polynomial<Complex>)other);
        }

        return null;
    }

    /**
     *
     */
    public Polynomial<N> subtract(Polynomial<N> other)
    {
        if (isInteger && other.isInteger) {
            return (Polynomial<N>)PolynomialInteger.subtract((Polynomial<Integer>)this, (Polynomial<Integer>)other);
        }

        if (isComplex && other.isComplex) {
            return (Polynomial<N>)PolynomialComplex.subtract((Polynomial<Complex>)this, (Polynomial<Complex>)other);
        }

        return null;
    }

    /**
     *
     */
    public Polynomial<N> multiply(Polynomial<N> other)
    {
        if (isInteger && other.isInteger) {
            return (Polynomial<N>)PolynomialInteger.multiply((Polynomial<Integer>)this, (Polynomial<Integer>)other);
        }

        if (isComplex && other.isComplex) {
            return (Polynomial<N>)PolynomialComplex.multiply((Polynomial<Complex>)this, (Polynomial<Complex>)other);
        }

        return null;
    }

    /**
     *
     */
    public Polynomial<N> dotMultiply(Polynomial<N> other)
    {
        if (isInteger && other.isInteger) {
            return (Polynomial<N>)PolynomialInteger.dotMultiply((Polynomial<Integer>)this, (Polynomial<Integer>)other);
        }

        if (isComplex && other.isComplex) {
            return (Polynomial<N>)PolynomialComplex.dotMultiply((Polynomial<Complex>)this, (Polynomial<Complex>)other);
        }

        return null;
    }
}
